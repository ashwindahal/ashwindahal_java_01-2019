package com.cerotid.bank.model;

public class WireTransfer extends Transaction{
	private String intermediaryBankSWIFTCode;
	private String beneficiaryBankName;
	private String beneficiaryAccountNumber;
	
	
	public WireTransfer() {
		
	}
	
	public WireTransfer(String intermediaryBankSWIFTCode, String beneficiaryBankName, String beneficiaryAccountNumber) {
		this.intermediaryBankSWIFTCode = intermediaryBankSWIFTCode;
		this.beneficiaryBankName = beneficiaryBankName;
		this.beneficiaryAccountNumber = beneficiaryAccountNumber;
	}
	
	public String getIntermediaryBankSWIFTCode() {
		return intermediaryBankSWIFTCode;
	}
	public void setIntermediaryBankSWIFTCode(String intermediaryBankSWIFTCode) {
		this.intermediaryBankSWIFTCode = intermediaryBankSWIFTCode;
	}
	public String getBeneficiaryBankName() {
		return beneficiaryBankName;
	}
	public void setBeneficiaryBankName(String beneficiaryBankName) {
		this.beneficiaryBankName = beneficiaryBankName;
	}
	public String getBeneficiaryAccountNumber() {
		return beneficiaryAccountNumber;
	}
	public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
		this.beneficiaryAccountNumber = beneficiaryAccountNumber;
	}

	@Override
	public String toString() {
		return super.toString() + " WireTransfer [intermediaryBankSWIFTCode=" + intermediaryBankSWIFTCode + ", beneficiaryBankName="
				+ beneficiaryBankName + ", beneficiaryAccountNumber=" + beneficiaryAccountNumber + "]";
	}
	
}
