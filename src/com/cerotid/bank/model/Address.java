package com.cerotid.bank.model;

import java.io.Serializable;

public class Address implements Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3637206354993650052L;
	private String addressLine;
	private String city;
	//validation (Either XXXXX or XXXXX-XXXX)
	private String zipCode;
	//Make sure all Caps; Max/Min Length 2.
	private String stateCode;
	
	public Address(String addressLine, String city, String zipCode, String stateCode) {
		super();
		this.addressLine = addressLine;
		this.city = city;
		this.zipCode = zipCode;
		this.stateCode = stateCode;
	}
	
	public String getAddressLine() {
		return addressLine;
	}
	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	@Override
	public String toString() {
		return "Address [addressLine=" + addressLine + ", city=" + city + ", zipCode=" + zipCode + ", stateCode="
				+ stateCode + "]";
	}
	
	
	
	
}
