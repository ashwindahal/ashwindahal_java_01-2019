package com.cerotid.bank.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Customer implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2713843867520012404L;
	private String firstName;
	private String lastName;
	private String ssn;
	private ArrayList<Account> accounts;
	private Address address;

	public Customer() {

	}

	public Customer(String firstName, String lastName, String ssn, Address address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.ssn = ssn;
		this.address = address;
	}

	// Accessors and mutators (Encapsulation)


	// behaviors
	void printCustomerAccounts() {
		// System.out.println(getAccounts());
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public ArrayList<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(ArrayList<Account> accounts) {
		this.accounts = accounts;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	void printCustomerDetails() {
		System.out.println(toString());
	}


	@Override
	public String toString() {
		return "Customer [firstName=" + firstName + ", lastName=" + lastName + ", ssn=" + ssn + ", accounts=" + accounts
				+ ", address=" + address + "]";
	}

	// lets us add a account to customer
	public void addAccount(Account account) {
		if (this.accounts == null) {
			this.accounts = new ArrayList<>();
		}

		this.accounts.add(account);
	}

}
