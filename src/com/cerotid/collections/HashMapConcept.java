package com.cerotid.collections;

import java.util.HashMap;

public class HashMapConcept {
	public static void main(String[] args) {
		HashMap<String , String> hashMap = new HashMap<String, String>();
		
		hashMap.put("123456789", "David Smith"); 
		hashMap.put("123456783", "Kim Clark"); 
		hashMap.put("123456782", "Randey Dewey"); 
		hashMap.put("123456781", "Michael Owen"); 
		hashMap.put("123456786", "Carla Garham"); 
		hashMap.put("123456788", "David Lewis"); 
		
		System.out.println(hashMap.get("123456789"));
		System.out.println(hashMap.get("123456783"));
		
		HashMap<String , String> stateMap = new HashMap<String, String>();
		stateMap.put("CA", "California"); 
		stateMap.put("TX", "Texas"); 
		stateMap.put("UT", "Utha");
		stateMap.put("NY","NewYork"); 
		stateMap.put("VA", "verginia");
		stateMap.put("CO", "Colorado");
		
		System.out.println(stateMap.get("CA"));
		
		System.out.println(stateMap.keySet());
		
		System.out.println(stateMap.isEmpty());
	}

}
