package com.cerotid.oo.principles;

public class Dog extends Pet {
	//constructor 
	 public Dog(){
		
	}
	//Overloaded constructor 
	Dog(boolean isInsured){
		super.setInsured(isInsured);
		super.setNumberOfTheeth(42);
	}
	Dog(boolean isSick, String color){
		super.setSick(isSick);
		super.setColor(color); 
	}
	
	@Override
	public void eat() {
		System.out.println("Dog treats, if i can");
	}
	
	public void eat(String food) {
		System.out.println("I am eating food " + food);
	}
	@Override
	public void sleepTime() {
		System.out.println("Its my Wish to sleep whenever i want");
	}

}
