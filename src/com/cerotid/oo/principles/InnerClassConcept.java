package com.cerotid.oo.principles;

public class InnerClassConcept {

	public static void main(String[] args) {
		InnerClassConcept inc = new InnerClassConcept();

		// instantiate inner class A
		//InnerA innerClassA = inc.new InnerA();

		// instantiate inner class B
		InnerB innerClassBObject = inc.new InnerB();
		innerClassBObject.accessInnerPropertyOfB();
		
		InnerA.accessInnerProperty();

	}

	public static class InnerA {
		public static void accessInnerProperty() {

		}
	}

	public class InnerB {
		public void accessInnerPropertyOfB() {

		}

	}

}
