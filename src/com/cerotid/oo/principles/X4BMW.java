package com.cerotid.oo.principles;

public class X4BMW extends Luxury {
	
	@Override
	public void driveSmooth() {
		super.driveSmooth();
	}

	@Override
	public void consumeGasoline() {
		super.consumeGasoline();
	}

	@Override
	public void setNumOfDoors(int numOfDoors) {
		super.setNumOfDoors(4);
		System.out.println("I have " + numOfDoors + "number of doors");
	}

	@Override
	public void move() {
		super.move();
		System.out.println("I am moving from the luxury class");
	}
	
	 

}
