package com.cerotid.oo.principles;

//abstract is just an idea
public class Vehicle {
	private Engine engine;
	private int numOfDoors; 
	private String Color;
	
	public Vehicle() {
		this.engine = new Engine();
	}

	public void consumeGasoline() {
		System.out.println("I am consumeing Gasoline, NOM NOM");
	}
	
	public void move() {
		System.out.println("I am moving, dont know where but i am moving");
	}
	
	public void makeEngineNoise() {
		System.out.println("My Engine is making noise");
	}
	
	public int getNumOfDoors() {
		return numOfDoors;
	}
	
	public void setNumOfDoors(int numOfDoors) {
		this.numOfDoors = numOfDoors;
	}
	
	public String getColor() {
		return Color;
	}
	
	public void setColor(String color) {
		Color = color;
	}
	
	
	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	@Override
	public String toString() {
		return "Vehicle [engine=" + engine + ", numOfDoors=" + numOfDoors + ", Color=" + Color + "]";
	}

	
}
