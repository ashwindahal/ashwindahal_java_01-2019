package com.cerotid.oo.principles;

public abstract class Animal {
	private String color;
	private final int limbs = 4;
	private int numberOfTheeth; //dont know this value yet
	private boolean isSick; 
	
	public abstract void sleepTime();
	
	
	public final void brethe() {
		System.out.println("Breathing in 02, breathing out CO2");
	}
	
	public void eat() {
		System.out.println("Eating food");
	}
	
	public void makeSound() {
		System.out.println("I dont know which sound to produce");
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getNumberOfTheeth() {
		return numberOfTheeth;
	}

	public void setNumberOfTheeth(int numberOfTheeth) {
		this.numberOfTheeth = numberOfTheeth;
	}

	public int getLimbs() {
		return limbs;
	}
	

	public boolean isSick() {
		return isSick;
	}

	public void setSick(boolean isSick) {
		this.isSick = isSick;
	}

	@Override
	public String toString() {
		return "Animal [color= " + color + ", limbs= " + limbs + ", numberOfTheeth= " + numberOfTheeth + "]";
	}

	

}
