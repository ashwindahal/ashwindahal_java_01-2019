package com.cerotid.oo.principles;

public class Engine {
	private int numberOfCylinders;
	private String maker;
	
	public int getNumberOfCylinders() {
		return numberOfCylinders;
	}
	public void setNumberOfCylinders(int numberOfCylinders) {
		this.numberOfCylinders = numberOfCylinders;
	}
	public String getMaker() {
		return maker;
	}
	public void setMaker(String maker) {
		this.maker = maker;
	}
}
