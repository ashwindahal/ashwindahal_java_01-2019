package com.cerotid.oo.principles;

public class F12Ferrari extends SportsCar {
	

	@Override
	public void driveFast() {
		super.driveFast();
	}

	public void speed(){
		System.out.println("I am a F12, i go from 0-60 in 1.5 Seconds");
	}

	@Override
	public String toString() {
		return "F12Ferrari [getNumOfDoors()=" + super.getNumOfDoors() + ", getColor()=" + getColor() + "]";
	}

}
