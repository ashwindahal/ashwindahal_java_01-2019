package com.cerotid.oo.principles;

public class VehicleTest {

	public static void main(String[] args) {
		//creating vehicle object 
		System.out.println("I am the first Vehicle");
		Vehicle vehicle = new Vehicle();
		vehicle.consumeGasoline();
		vehicle.makeEngineNoise();
		vehicle.move();
		VehicleServiceCenter.repairVehicle(vehicle);
		System.out.println();
		
		
		//creating vehicle object 
		System.out.println("I am a Sports Vehicle #1");
		SportsCar vehicle2 = new SportsCar();
		vehicle2.consumeGasoline();
		vehicle2.driveFast();
		vehicle2.move();
		vehicle2.getNumOfDoors();
		System.out.println(vehicle2.feature);
		VehicleServiceCenter.repairVehicle(vehicle2);
		System.out.println();
		
		//creating vehicle object 
		System.out.println("I am a Sports Vehicle#2");
		SportsCar sp = new SportsCar();
		sp.consumeGasoline();
		sp.driveFast();
		sp.move();
		sp.getNumOfDoors();
		System.out.println(sp.feature);
		VehicleServiceCenter.repairVehicle(sp);
		System.out.println();
		
		sp.feature = "crazy-rider";
		System.out.println(vehicle2.feature);
		System.out.println();
		
		
		
		//creating vehicle object 
		System.out.println("I am a luxury Vehicle");
		Luxury vehicle3 = new Luxury(); 
		vehicle3.consumeGasoline();
		vehicle3.driveSmooth();
		vehicle3.move();
		VehicleServiceCenter.repairVehicle(vehicle3);
		System.out.println();
		
		//creating F12Ferrari object and testing toString Method 
		F12Ferrari ferrari = new F12Ferrari();
		ferrari.toString();
		VehicleServiceCenter.repairVehicle(ferrari);
		System.out.println();
		
		//creating a New sports Vehicle of type Vehicle (polymorphism)
		Vehicle sportVehicle = new SportsCar(); //Waiting for dai for good ex // its for felxibility - but how??
		sportVehicle.consumeGasoline(); //overriding
		VehicleServiceCenter.repairVehicle(sportVehicle);
		
		
	
		

	}

}
