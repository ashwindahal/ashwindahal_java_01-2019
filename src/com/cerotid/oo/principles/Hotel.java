package com.cerotid.oo.principles;

import com.cerotid.oo.interfaceconcept.VendingMachine;

public class Hotel implements VendingMachine{
	private int numberOfRooms;
	private boolean gym;
	private boolean pool;
	
	
	public boolean isPool() {
		return pool;
	}
	public void setPool(boolean pool) {
		this.pool = pool;
	}
	public int getNumberOfRooms() {
		return numberOfRooms;
	}
	public void setNumberOfRooms(int numberOfRooms) {
		this.numberOfRooms = numberOfRooms;
	}
	public boolean isGym() {
		return gym;
	}
	public void setGym(boolean gym) {
		this.gym = gym;
	}
	@Override
	public void displayVendingMachine() {
		System.out.println("Want to buy Sodas");
	}
	

}
