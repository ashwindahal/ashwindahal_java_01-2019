package com.cerotid.oo.principles;

public class SportsCar extends Vehicle {
	static String feature;
	//static block - optional
	static {
		System.out.println("I am in static block of SportsCar");
		feature = "low-rider";
	}
	
	//constructor - default constructor
	public SportsCar() {
		installNumOfDoors();
		super.setNumOfDoors(2);
		// TODO Auto-generated constructor stub
	}
	
	
	//overloaded constructor
	public SportsCar(String color) {
		super.setColor(color);
		installNumOfDoors();
	}
	
	
	public void driveFast(){
		System.out.println("I am a sports car i drive fast! vroom vroom");
	}

	public void performPremiumService() {
		System.out.println("Premium service provided");
	}

	@Override
	public void consumeGasoline() {
		System.out.println("I take plus grade gasoline");
	}
	
	private void installNumOfDoors() {
		super.setNumOfDoors(2);
		System.out.println("I have " + getNumOfDoors() + "number of doors");
	}
	
	@Override
	public void move() {
		super.move();
		System.out.println("I am moving from the Sports class");
	}


	 

}
