package com.cerotid.oo.principles;

public class VehicleServiceCenter {
	
	
	//Polymorphism= nultiple forms
	//Vehicle = SportsCarr, LuxuryCar, F12Ferrari, X4BMW or anything in the future that is Vehicle
	
	public static void repairVehicle(Vehicle vehicle) {
		//Abstraction
		
		//In between process that consumer don't need to know is abstraction
		//Abstraction is not a abstract class
		//Abstraction is hiding the complexity
		
		if(vehicle instanceof SportsCar) {
			((SportsCar) vehicle).performPremiumService(); //down-cast
			SportsCar sc = (SportsCar) vehicle;
			
			System.out.println("Assigned repairperson who knows how to work on Sports car "+ sc.toString());
		}
		
		System.out.println(vehicle.getClass().getSimpleName() + " repaired. Thank you for visitng us!" );
	}
}
