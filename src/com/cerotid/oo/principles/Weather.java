package com.cerotid.oo.principles;

public enum Weather {
	//CHECKING, SAVING, BUSINESS_CHECKING for Assignment 
	SUMMER("summer"), WINTER("winter"), FALL("fall"), SPRING("spring");
	
	private final String weather;
	
	Weather(String a){
		this.weather = a; 
	}

	public String getWeather() {
		return weather;
	}
}
