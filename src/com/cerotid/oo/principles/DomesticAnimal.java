package com.cerotid.oo.principles;

public class DomesticAnimal extends Animal {
	public void lives() {
		System.out.println("I live around people");
	}
	
	public void eat() {
		System.out.println("I eat whatever people feed me");
	}

	@Override
	public void sleepTime() {
		System.out.println("Whenever I am not eting");
	}

}
