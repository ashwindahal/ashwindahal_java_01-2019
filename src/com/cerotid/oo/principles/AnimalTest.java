package com.cerotid.oo.principles;

import com.cerotid.oo.animalaction.Veterinary;

public class AnimalTest {

	public static void main(String[] args) {
		//Creating Animal Object 
		System.out.println("I am first Animal");
		Animal animal1 = new Zebra();
		animal1.brethe();
		animal1.eat();
		animal1.makeSound();
		
		
		
		//Create Wild Animal Object
		System.out.println("I am first Wild Animal");
		WildAnimal wildAnimal1 = new Zebra ();
		wildAnimal1.brethe();
		wildAnimal1.eat();
		wildAnimal1.makeSound();
		wildAnimal1.lives();
		
		System.out.println();
		System.out.println("I am a Lion");
		Lion lion = new Lion();
		lion.eat();
		
		//Animal = Dog is also an animal
		Animal animal2 = new Dog();
		Animal animal3 = wildAnimal1; 
		
		
		Dog dog = new Dog(true,"White");
		System.out.println("I am Insured " + dog.isInsured() + " & I have this many teeth: "
				+ dog.getNumberOfTheeth());
		
		Dog dog2 = new Dog(); 
		dog2.eat("Dog Treates");
		
		
		//Dog != Animal, because Animal is not nessarily a dog
		//Dog dog = new Animal();
		
		Animal cowAnimal = new Cow();
		cowAnimal.eat();
		
		Cow cow = new Cow();
		cow.eat();
		
		Animal aCow = cow; 
		
		Object a = aCow; 
		
		Veterinary veterinary = new Veterinary();
		veterinary.treatAnimal(dog);
		veterinary.treatAnimal(cow);
		veterinary.treatAnimal(lion); 
		veterinary.treatAnimal(wildAnimal1); 
		
		//veterinary.();
		//Petsamrt newLocation = new PetSamrt(VendingMachine SnacksVendingMachine)
		
		
	}

}
