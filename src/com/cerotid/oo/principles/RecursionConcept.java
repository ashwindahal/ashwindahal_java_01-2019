package com.cerotid.oo.principles;

public class RecursionConcept {
	
	//calculate factorial
	//!3 = 3*2*1 = 6;, !5 = 5*4*3*2*1 = 120
	public static int factorial(int number) {		
		if(number == 0)
		{
			return 1;
		}
		else 
			return number * factorial(number -1);
		
		//5*factorial(4)
		//4*factorial(3)
		//3*Factorial(2)
		//2*Factorial(1)
		//1*factorial(0)
		
	}
	
	public static int calculateFactorialUsingLoop(int num) {
		int result = num;
		for(int i = num - 1; i > 0; i--) {
			result = result * i;
		}
		
		return result;
		
	}
	
	public static void main(String[] args) {
		System.out.println(RecursionConcept.calculateFactorialUsingLoop(5));
		
		System.out.println(RecursionConcept.factorial(5));
	}
	
}
