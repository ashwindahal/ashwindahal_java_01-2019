package com.cerotid.oo.animalaction;

import com.cerotid.oo.interfaceconcept.CokeVendeingMachine;
import com.cerotid.oo.interfaceconcept.MuscleMilkVendingMachine;
import com.cerotid.oo.interfaceconcept.VendingMachine;

public class PetSmartTest {

	public static void main(String[] args) {
		VendingMachine vendingMachine = new CokeVendeingMachine();
		
		Petsmart petsmart = new Petsmart(vendingMachine);
		petsmart.getVendingMachine().displayVendingMachine();

		VendingMachine muscleMilkVendingMachine = new MuscleMilkVendingMachine();
		petsmart.setVendingMachine(muscleMilkVendingMachine);
		petsmart.getVendingMachine().displayVendingMachine();

		
	}

}
