package com.cerotid.oo.animalaction;

import com.cerotid.oo.interfaceconcept.PetsmartFranchiser;
import com.cerotid.oo.interfaceconcept.VendingMachine;
import com.cerotid.oo.principles.Animal;

public class Petsmart implements PetsmartFranchiser {

	private VendingMachine vendingMachine;

	Petsmart(VendingMachine vendingMachineFranchiser) {
		this.vendingMachine = vendingMachineFranchiser;
	}

	public VendingMachine getVendingMachine() {
		return vendingMachine;
	}

	public void setVendingMachine(VendingMachine vendingMachine) {
		this.vendingMachine = vendingMachine;
	}

	@Override
	public void registerAnimal(Animal animal) {
		System.out.println(animal.getClass() + " Registered Petsmart way");

	}

	@Override
	public Animal treatAnimal(Animal animal) {
		System.out.println("Sorry " + animal.getClass() + " treated Petsmart way");
		return null;
	}

	@Override
	public void groomAnimal(Animal animal) {
		System.out.println("Sorry " + animal.getClass() + " Grommed Petsmart way");

	}

}
