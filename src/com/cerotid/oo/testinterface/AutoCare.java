package com.cerotid.oo.testinterface;

public class AutoCare {
	private boolean hasTireChangeService;
	private boolean hasOilChangeService;

	public AutoCare(boolean hasTireChangeService, boolean hasOilChangeService) {
		this.hasTireChangeService = hasTireChangeService;
		this.hasOilChangeService = hasOilChangeService;
	}

	public boolean isHasTireChangeService() {
		return hasTireChangeService;
	}

	public void setHasTireChangeService(boolean hasTireChangeService) {
		this.hasTireChangeService = hasTireChangeService;
	}

	public boolean isHasOilChangeService() {
		return hasOilChangeService;
	}

	public void setHasOilChangeService(boolean hasOilChangeService) {
		this.hasOilChangeService = hasOilChangeService;
	}

	@Override
	public String toString() {
		return "AutoCare [hasTireChangeService=" + hasTireChangeService + ", hasOilChangeService=" + hasOilChangeService
				+ "]";
	}

}
