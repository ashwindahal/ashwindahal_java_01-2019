package com.cerotid.oo.testinterface;

import com.cerotid.oo.interfaceconcept.VendingMachine;

public class CarWash {
	private boolean isOpen;
	private VendingMachine vendingMachine; 
	
	public CarWash() {
		
	}
	public CarWash(VendingMachine vendingMachine) {
		this.vendingMachine = vendingMachine; 
		
	}
	public boolean isOpen() {
		return isOpen;
	}

	public void setOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}
	
}
