package com.cerotid.oo.testinterface;

public class WallMartTester {

	public static void main(String[] args) {
		VendingMachines cokeVendingMachine = new CokeVendingMachine();
		
		WallMart wallmart1 = new WallMart(cokeVendingMachine);
		wallmart1.getWallMartVendingMachine().provideVendingMachine(); //why not print snack? by default
		
		WallMart wallmart2 = new WallMart();
		VendingMachines wallmartSnackVendingMachine = new SnacksVendingMachine();
		wallmart2.setWallMartVendingMachine(wallmartSnackVendingMachine);
		wallmart2.getWallMartVendingMachine().provideVendingMachine(); 
		
		WallMart wallmart3 = new WallMart();
		VendingMachines wallmartCokeVendingMachine = new CokeVendingMachine();
		wallmart3.setWallMartVendingMachine(wallmartCokeVendingMachine);
		wallmart3.getWallMartVendingMachine().provideVendingMachine();
		
	
	}

}
