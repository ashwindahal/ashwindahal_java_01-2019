package com.cerotid.oo.testinterface;

public class CokeVendingMachine implements VendingMachines {

	@Override
	public void provideVendingMachine() {
		System.out.println("I am vending coke");
	}

}
