package com.cerotid.oo.testinterface;

public class WallMart {
	private VendingMachines vendingMachine;

	public WallMart() {

	}

	public WallMart(VendingMachines vendingMachine) {
		this.vendingMachine = vendingMachine;
	}

	public VendingMachines getWallMartVendingMachine() {
		return vendingMachine;
	}

	public void setWallMartVendingMachine(VendingMachines vendingMachine) {
		this.vendingMachine = vendingMachine;
	}

}
