package com.cerotid.oo.testinterface;

public class SnacksVendingMachine implements VendingMachines {

	@Override
	public void provideVendingMachine() {
		System.out.println("I am vending snacks");
	}

}
