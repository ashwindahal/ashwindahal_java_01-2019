package com.cerotid.oo.testinterface;

public class NeighborHoodWallMart extends WallMart {
	private boolean hasPharmacy;

	public NeighborHoodWallMart(boolean hasPharmacy) {
		super();
		this.hasPharmacy = hasPharmacy;
	}

	public boolean isHasPharmacy() {
		return hasPharmacy;
	}

	public void setHasPharmacy(boolean hasPharmacy) {
		this.hasPharmacy = hasPharmacy;
	}

}
