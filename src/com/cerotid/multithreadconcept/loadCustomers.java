package com.cerotid.multithreadconcept;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
public class loadCustomers {
	public static void main(String[] args){
		Thread t1 = new Thread(new MultiThread());
		Thread t2 = new Thread(new MultiThread());
		//start of threads,  basically reads the same text file with two threads
		t1.start();
		t2.start();
		
		
		//total we have three threads in action, main, t1, and t2
	}

}

class MultiThread implements Runnable {
	//creating a empty object to read file
	private static BufferedReader bufferReader = null;
	//creating a empty list to add customer information  
	private List<String> list;
	//making a static block to use the bufferreader and reading file "Customers.txt"
	static {
		try {
			bufferReader = new BufferedReader(new FileReader("Customers.txt"), 10);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//Overriding the run method in Runnable class  
	public void run() {
		String line = null;
		int count = 0;
		while (true) {
			//creating a array list
			this.list = new ArrayList<String>();
			//using synchronized to ensure threads are in order 
			
			synchronized (bufferReader) {
				try {
					while ((line = bufferReader.readLine()) != null) {
						if (count < 15) {
							list.add(line);
							count++;
						} else {
							list.add(line);
							count = 0;
							break;
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			try {
				//sleeps for 1000 mili seconds 
				Thread.sleep(500);
				//displays list after 1000 mili seconds 
				display(this.list);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//reads line and breaks program line is null or empty
			if (line == null)
				break;
		}

	}

	public void display(List<String> list) {
		//prints customers from the list that was read 
		for (String customers : list) {
			System.out.println(customers);
		}
		//prints the size of the list using as a checker 
		System.out.println(list.size());
	}

}