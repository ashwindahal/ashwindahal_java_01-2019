package com.cerotid.client;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.util.Scanner;
import com.cerotid.bank.bo.BankBO;
import com.cerotid.bank.bo.BankBOImpl;
import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.AccountType;
import com.cerotid.bank.model.Address;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.DeliveryOption;
import com.cerotid.bank.model.ElectronicCheck;
import com.cerotid.bank.model.MoneyGram;
import com.cerotid.bank.model.ReadBankCustomers;
import com.cerotid.bank.model.Transaction;
import com.cerotid.bank.model.TypeOfCheck;
import com.cerotid.bank.model.WireTransfer;

public class BankUI {
	private BankBO bankBO; // had to change to static dont know why

	public static void main(String[] args) {
		BankUI bankUI = new BankUI();
		bankUI.setBankBO(new BankBOImpl()); // we could implement any controller gives us flexibility
		// bankUI.setBankBO(new AshwinBank());
		// will always run is infinite loop
		
		
		ReadBankCustomers readBankCustomers = new ReadBankCustomers(bankUI.bankBO.getBank()); 
		readBankCustomers.loadBank();
		
		while (true) {
			printDisplayOptionMenu();
			bankUI.actionPerformedByUserInput();
		}

	}

	private static void printDisplayOptionMenu() {
		System.out.println("Choose from the menu below:\r\n" + "1. Add Customer\r\n" + "2. Add Account\r\n"
				+ "3. Send Money\r\n" + "4. Print Bank Status\r\n" + "5. Get Customers by Sate Code" + "\n6. Exit");
	}

	public static Scanner getScannerInput() {
		Scanner scan = new Scanner(System.in);
		return scan;
	}

	public void addCustomer() {
		Scanner customerInputReader = getScannerInput();
		System.out.println("Enter Customer First Name: ");
		String firstName = customerInputReader.next();
		System.out.println("Enter Customer Last Name: ");
		String lastName = customerInputReader.next();
		System.out.println("Enter Customer SSN: ");
		String ssn = customerInputReader.next();
		System.out.println("Enter Customer Address Line: ");
		String addressLine = customerInputReader.next();
		addressLine += customerInputReader.nextLine();
		System.out.println("Enter Customer City: ");
		String city = customerInputReader.next();
		System.out.println("Enter Customer Zip Code: ");
		String zipCode = customerInputReader.next();
		System.out.println("Enter Customer State Code: ");
		String stateCode = customerInputReader.next();
		System.out.println("Customer Add in progress!!");

		Address address = new Address(addressLine, city, zipCode, stateCode);
		Customer customer1 = new Customer(firstName, lastName, ssn, address);
		bankBO.addCustomer(customer1);

		System.out.println();

	}

	private void addCustomerAccount() {
		Customer customer = gatherCustomerInformation();
		/// help here

		if (customer != null) {
			//// read bank.ser file information and write it to the Customer object
			// and check if customer exits or not.
			AccountType accountType = getCustomerAccountTypeToStart();
			System.out.println("Provide Opening Balance in Account: ");
			Scanner accountBalance = getScannerInput();
			double openeingBalance = accountBalance.nextDouble();
			Account account = new Account(accountType, new Date(), openeingBalance);

			bankBO.openAccount(customer, account);
			customer.addAccount(account);
			System.out.println("Account Added");
			System.out.println(account);
		} else
			System.out.println("Customer not available!");

	}

	public void actionPerformedByUserInput() {
		try {
			getScannerInput();
			Scanner input = getScannerInput();
			int choice = 0;
			choice = input.nextInt();

			switch (choice) {
			case 1:
				addCustomer();
				break;
			case 2:
				addCustomerAccount();
				break;
			case 3:
				Transaction transaction = getTransaction();
				Customer customer = gatherCustomerInformation();

				bankBO.sendMoney(customer, transaction);

				break;
			case 4:
				bankBO.printBankStatus();
				
				break;
			case 5:
				System.out.println("API Under Construction");

				break;
			case 6:
				System.out.println("Thank you for choosing CEROTID BANK");
				serilizeBank(bankBO.getBank());
				break;
			default:
				System.out.println("Please choice Either 1, 2, 3, 4, or 5");
				printDisplayOptionMenu();
				break;
			}

		} catch (Exception ex) {
			System.out.println("Error occurred: " + ex.getMessage());
			ex.printStackTrace();
		}
	}

	private void serilizeBank(Object serObj) {
		try {
			FileOutputStream fileOut = new FileOutputStream("bank.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(serObj);
			System.out.println(serObj);
			out.close();
			fileOut.close();

			System.out.printf("Serialized data is saved as bank.ser ");
		} catch (IOException i) {
			i.printStackTrace();
		}
	}

	private Transaction getTransaction() {
		Transaction transaction = createUserSpecifiedTransaction();

		getGeneralTransactionInfo(transaction);

		return transaction;
	}

	private void getGeneralTransactionInfo(Transaction transaction) {
		Scanner userInput = getScannerInput();
		System.out.println("Enter recivers First Name:");
		String firstName = userInput.next();
		System.out.println("Enter recivers Last Name:");
		String lastName = userInput.next();
		System.out.println("Enter Amount to Send:");

		double amount = userInput.nextDouble();
		double fee = 0;

		if (amount <= 10) {
			fee = 1;
		} else if ((amount >= 10) && (amount < 100)) {
			fee = 3;
		} else {
			fee = 6;
		}

		transaction.setReceiverFirstName(firstName);
		transaction.setFee(fee);
		transaction.setAmount(amount);
		transaction.setReceiverLastname(lastName);

		System.out.println(transaction.toString());
		System.out.println("Transaction Fee is: $" + fee);
		System.out.println("Transaction Completed!");
	}

	private Transaction createUserSpecifiedTransaction() {
		Transaction transaction = null;

		displayTransactionTypeOptions();
		try {
			Scanner input = getScannerInput();

			int choice = input.nextInt();

			switch (choice) {
			case 1:
				receiveWireTransferTransactionInfo(transaction); // Pass by reference
				break;
			case 2:
				receiveMonetGramTransactionInfo(transaction);
				break;
			case 3:
				receiveElectronicTypeTransactionInfo(transaction);
				break;
			default:
				System.out.println("Choose Options 1, 2, or 3");
				displayTransactionTypeOptions();
				break;
			}

		} catch (Exception ex) {
			System.out.println("Error occurred: " + ex.getMessage());
			ex.printStackTrace();
		}

		return transaction;
	}

	private void receiveElectronicTypeTransactionInfo(Transaction transaction) {
		Scanner input = getScannerInput();

		System.out.println("What type of check to transfer? Choose below");
		System.out.println("1. Paper Check");
		System.out.println("2. Electronic Check");

		TypeOfCheck typeOfCheck = null;
		int choice3 = input.nextInt();
		if (choice3 == 1) {
			typeOfCheck = TypeOfCheck.paperCheck;
		} else if (choice3 == 2) {
			typeOfCheck = TypeOfCheck.eCheck;
		}
		transaction = new ElectronicCheck();

		ElectronicCheck typeOfCheck1 = (ElectronicCheck) transaction;
		typeOfCheck1.setTypeOfCheck(typeOfCheck);
		System.out.println(typeOfCheck1.toString());
	}

	private void displayTransactionTypeOptions() {
		System.out.println("What Type of Transfer Transaction does the Customer need? Choose below:");
		System.out.println("1. Wire Transfer");
		System.out.println("2. Money Gram");
		System.out.println("3. Electronic Check");
	}

	private void receiveMonetGramTransactionInfo(Transaction transaction) {
		Scanner input = getScannerInput();

		System.out.println("What method of Delivery does the Customer perfer? Choose below:");
		System.out.println("1. 10 Minutes");
		System.out.println("2. 24 Hours");

		DeliveryOption deliveryTime = null;
		int choice2 = input.nextInt();
		if (choice2 == 1) {
			deliveryTime = DeliveryOption.TEN_MINUTES;
		} else if (choice2 == 2) {
			deliveryTime = DeliveryOption.TWENTY_FOUR_HOURS;
		}

		System.out.println("What is Destination country of this Transfer?");
		String destinationCountry = input.next();
		// creating MoneyGram Object and setting delivery time and Destination Country
		transaction = new MoneyGram();

		MoneyGram moneyGram = (MoneyGram) transaction;
		moneyGram.setDeliveryOptions(deliveryTime);
		moneyGram.setDestinationCountry(destinationCountry);
		System.out.println(moneyGram.toString());
	}

	private void receiveWireTransferTransactionInfo(Transaction transaction) {
		Scanner input = getScannerInput();

		System.out.println("Enter Intermediary Bank Swift Code:");
		String intermediaryBankSWIFTCode = input.next();
		System.out.println("Enter Beneficiary Bank Name:");
		String beneficiaryBankName = input.next();
		System.out.println("Enter Beneficiary Account Number:");

		String beneficiaryAccountNumber = input.next();

		transaction = new WireTransfer();

		WireTransfer wireTransfer = (WireTransfer) transaction;
		wireTransfer.setIntermediaryBankSWIFTCode(intermediaryBankSWIFTCode);
		wireTransfer.setBeneficiaryBankName(beneficiaryBankName);
		wireTransfer.setBeneficiaryAccountNumber(beneficiaryAccountNumber);
		System.out.println(wireTransfer.toString());
	}

	private Customer gatherCustomerInformation() {
		Scanner customerInputReader = getScannerInput();
		System.out.println("Enter Customer SSN to access Account information: ");
		String ssn = customerInputReader.next();
		return bankBO.getCustomerInfo(ssn);
	}

	private void displayAccountTypeChoice() {
		System.out.println("Choose AccountType: [By default is Checking account]: ");
		System.out.println("1. Checking 2. Saving 3. Business Checking");
	}

	private AccountType getCustomerAccountTypeToStart() {
		displayAccountTypeChoice();

		AccountType accountType = null;
		Scanner accountTypeInput = getScannerInput();

		switch (accountTypeInput.nextInt()) {
		case 1: {
			accountType = AccountType.CHECKING;
		}
		case 2: {
			accountType = AccountType.SAVING;
		}
		case 3: {
			accountType = AccountType.BUSINESS_CHECKING;
		}
		default: {
			accountType = AccountType.CHECKING;
		}
			return accountType;
		}

	}

	public void setBankBO(BankBO bankBO) {
		this.bankBO = bankBO;
	}
}
