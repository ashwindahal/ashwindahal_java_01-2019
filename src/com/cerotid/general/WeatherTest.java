package com.cerotid.general;

import com.cerotid.oo.principles.Weather;

public class WeatherTest {

	public static void main(String[] args) {
		System.out.println(Weather.FALL);
	
		
		Weather weather = Weather.SPRING;
		
		switch(weather) {
		case SUMMER:
			System.out.println("Go Vacation!");
			break;
			
		case WINTER:
			System.out.println("Infront of fire place!");
			break;
			
		case FALL:
			System.out.println("Enjoy Fall Color!");
			break;
		
		case SPRING:
			System.out.println("Party!");
			break;
			
		default:
			break; 
	
		}
		
		System.out.println(Weather.valueOf("SUMMER"));
		
	}
	
}
	
	
