/*
	3. ClassName = PrimeNumberFinder
	Print prime number from 1 to 100.
	Hit: Use %
	
	Example:  Prime number from 2-10 are 2, 3, 5, 7
	
	2, 3, 5, 7
	git commit
*/

package com.cerotid.general;

public class PrimeNumberFinder {

	public static void main(String[] args)
	{
		
		 int i = 0;
	     int number = 0;
		
	     for (i = 1; i <= 100; i++)         
	       { 		  	  
	          int count = 0; 	  
	          for(number =i; number >= 1; number--)
	          {
	             if(i % number == 0)
	             	{
	            	 	count++;
	             	}
	          }
	          if (count ==2)
	          {
	        	  System.out.println(i);
	          }	
	     }	
	}
	
}
 
	
