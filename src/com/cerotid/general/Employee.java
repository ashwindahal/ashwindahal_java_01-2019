/*
		1. Create Employee class with variables name, ssn, salary and behavior as printEmployeeInfo, printSocialSecurityandName
		Main method= Create 2 employee objects
		Use printEmployeeInfo and printSocialSecurityandName
		
	*/

package com.cerotid.general;

public class Employee {
	// Properties or variables or state
	public String name;
	public String ssn;
	public double salary;

	public Employee() {
	}
	
	public Employee(String name, String ssn, double salary) {
		this.name = name;
		this.ssn = ssn;
		this.salary = salary;
	}

	// behaviors// function //worker
	// [modifier] [return Type] [methodName- action] [arguments or paramteres)
	public String printEmployeeInfo() {
		return ("Name: " + name + "\nSocial Security Number: " + ssn + "\nSalary: " + salary);
	}

	public String printSocailSecurityandName() {
		return ("Name: " + name + "\nSocial Security Number: " + ssn);
	}
	
	//compile time error vs runtime error

	public static void main(String args[]) {
		Employee employeeInfo = new Employee(); // instantiation --> assignment
		System.out.println(employeeInfo);
		employeeInfo.name = "Ashwin Dahal";
		employeeInfo.ssn = "123";
		employeeInfo.salary = 123456.89;

		String person1 = employeeInfo.printEmployeeInfo(); // calling or invoking a method
		System.out.println(person1);
		
		System.out.println();

		System.out.println(employeeInfo);

		Employee socailSecurityandName = new Employee();
		socailSecurityandName.name = "David Sanchez";
		socailSecurityandName.ssn = "1578";
		socailSecurityandName.salary = 10000000.89;

		String person2 = socailSecurityandName.printSocailSecurityandName();
		System.out.println(person2);
		
		Employee[] empArray = new Employee[2];
		empArray[0] = employeeInfo;
		empArray[1] = socailSecurityandName; 
		
		
		

	}
	
	

	@Override
	public String toString() {
		return "Employee [name=" + name + ", ssn=" + ssn + ", salary=" + salary + "]";
	}
	
	//Alt+Shift+S
	

}
