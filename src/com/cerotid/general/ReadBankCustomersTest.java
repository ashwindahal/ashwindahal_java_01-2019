package com.cerotid.general;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import com.cerotid.bank.model.Address;
import com.cerotid.bank.model.Customer;

public class ReadBankCustomersTest {

	ArrayList<Customer> customerList = new ArrayList<Customer>();

	Scanner reader;

	public ReadBankCustomersTest() {
		initialize("Customers.txt");
	}

	// overloaded constructor
	public ReadBankCustomersTest(String fileName) {
		initialize(fileName);

	}

	private void initialize(String fileName) {
		try {
			reader = new Scanner(new File(fileName));
		} catch (FileNotFoundException e) {
			System.out.println("File not Found, " + e.getMessage());
			e.printStackTrace();
		}
	}

	// create method to read file
	public void readFile() {
		while (reader.hasNext()) {
			String customerLine = reader.nextLine();
			addCustomer(splitLineIntoCustomerDetailsArray(customerLine)); //start thread 1 reader, 2 processor, 3 writer 
		}
		reader.close();
	}

	public String[] splitLineIntoCustomerDetailsArray(String customerLine) {
		String[] customerInfos = customerLine.split(","); 
		return customerInfos;
	}
	// create writer file
	public void writeFile() throws IOException {
		// sortCustomers(customerList);
		FileWriter writer = new FileWriter("test.txt");
		if (customerList != null) {
			for (Customer customer : customerList) {
				writer.write(customer.getFirstName() + "," + customer.getLastName() + "," + customer.getAddress() + "\n");
			}

		}
		writer.close();
	}

	/*
	 * // sorts an ArrayList of type Customer using "Collections.sort" private void
	 * sortCustomers(ArrayList<Customer> customerList) {
	 * Collections.sort(customerList,(o1, o2) -> { Customer c1 = (Customer) o1;
	 * Customer c2 = (Customer) o2; int res =
	 * c1.getFirstName().compareToIgnoreCase(c2.getFirstName()); if (res != 0)
	 * return res; return c1.getLastName().compareToIgnoreCase(c2.getLastName());
	 * }); }
	 */

	// Clean Code
	public void addCustomer(String[] customerInfos) {
		Address address = new Address(customerInfos[2], customerInfos[3], customerInfos[4], customerInfos[5]);
		Customer customer = new Customer(customerInfos[0], customerInfos[1], null, address);
		customerList.add(customer);
	}

}
