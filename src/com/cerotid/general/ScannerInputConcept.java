package com.cerotid.general;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ScannerInputConcept {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		int i = 1;
		getScannerInput();

		do {
			System.out.println("What is your name?: ");

			String name = sc.nextLine();
			printUserInput(name);

			System.out.println("What is your age: ");
			int printage = sc.nextInt(); 
			printUserInputInt(printage); 
			
			int age = 0; 
			while(true) 
			{
				try
				{
					getScannerInput(); 
					age = sc.nextInt();
					break; //will break the immediate loop
				}
				catch(InputMismatchException e) {
					System.out.println("Enter a value type int");
				}
			}
			
			printUserInput(name);
			printUserInputInt(age);

			i++;

		}
		while (i <= 5);

	}

	private static void getScannerInput() {
			sc = new Scanner(System.in);

	}

	private static void printUserInput(String name) {
		System.out.println("You entered: " + name);
	}
	private static void printUserInputInt(int age) {
		System.out.println("You entered: " + age);
	}

}
