package com.cerotid.general;

public class StateLookup {
	//class variable
	static int classVariable = 10;
	
	//key-value
	
	//instance variable 
	String [][] stateArray = {{"TX", "Texas"}, {"CA", "California"}, {"CO", "Colorado"}  };
	
	//constructor is for instatiation and variable assignment
	//Java class has default constructor
	
	//CO
	public String getState(String stateAbbreviation) {
		String state = null;
		
		for (int i = 0; i < stateArray.length; i++) {
			if(stateArray[i][0] == stateAbbreviation) {
				state = stateArray[i][1];
			}
		}
		
		//Ctrl+Shift+/
		
		/*
		 * for(int i = 0; i < 2; i++) { for (int j = 0; j < 2; j++) { if(stateArray[][])
		 * {
		 * 
		 * } } }
		 * 
		 */
		
		
		

		
		
		
		return state;
	}
	
	public static void main(String[] args) {
		StateLookup statelookup = new StateLookup(); //create an instance--> instance variable
		
		System.out.println("State:"+ statelookup.stateArray[0][1]);
		
		System.out.println(statelookup.getState("TX"));
		System.out.println(statelookup.getState("CO"));
		System.out.println(statelookup.getState("NY"));
		System.out.println(statelookup.getState("CA"));
		System.out.println(statelookup.getState("MD"));
		System.out.println(statelookup.getState("OK"));
		
		System.out.println(StateLookup.classVariable);
	}
	
}
